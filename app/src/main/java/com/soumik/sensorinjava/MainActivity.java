package com.soumik.sensorinjava;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.soumik.android.custompopup.Popup;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private TextView textView;
    private TextView textView2;
    private LinearLayout linearLayout;

    private SensorManager sensorManager;

    private Sensor accelerometerSensor;
    private Sensor proximitySensor;
    private Sensor lightSensor;
    private Sensor stepCounterSensor;
    private Sensor tempSensor;
    private Sensor gyroscopeSensor;
    private Sensor rotationSensor;
    private Sensor linearAccelerationSensor;
    private Sensor gravitySensor;

    private boolean run = false;

    private int currentSensor;

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 600;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.tvResult);
        textView2 = findViewById(R.id.tv_sensor_name);
        linearLayout = findViewById(R.id.layout_result);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            stepCounterSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        }
        gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        tempSensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        linearAccelerationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        gravitySensor= sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
    }

    public boolean checkSensorAvailability(int sensorType) {
        boolean isSensor = false;
        if (sensorManager.getDefaultSensor(sensorType) != null) {
            isSensor = true;
        }
        return isSensor;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == currentSensor) {

            linearLayout.setVisibility(View.VISIBLE);

            if (currentSensor == Sensor.TYPE_LIGHT) {
                float valueZ = event.values[0];
//                customDialog(this,"Light Sensor","Your Brightness Level is: "+valueZ);
                textView.setText("Brightness level is: " + valueZ);
                textView2.setText("Light Sensor");
            }
            else if (currentSensor == Sensor.TYPE_PROXIMITY) {
                float distance = event.values[0];
//                customDialog(this,"Light Sensor","Proximity is: "+distance);
                textView.setText("Proximity is: " + distance);
                textView2.setText("Proximity Sensor");
            }
            else if (currentSensor == Sensor.TYPE_STEP_COUNTER) {
               if (run){
                   float steps = event.values[0];
//                   customDialog(this,"Step Counter","Steps: "+steps);
                   textView.setText("Total Steps: " + steps);
                   textView2.setText("Step Counter Sensor");
               }
            }
            else if (currentSensor == Sensor.TYPE_ACCELEROMETER) {
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];

                textView2.setText("Accelerometer Sensor");
                textView.setText("X axis: "+x+"\nY axis: "+y+"\nZ axis: "+z);
                long curTime = System.currentTimeMillis();

                if ((curTime - lastUpdate) > 100) {
                    long diffTime = (curTime - lastUpdate);
                    lastUpdate = curTime;

                    float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;

                    if (speed > SHAKE_THRESHOLD) {
                        Toast.makeText(getApplicationContext(), "Your phone just shook", Toast.LENGTH_LONG).show();
                    }

                    last_x = x;
                    last_y = y;
                    last_z = z;

//                    customDialog(this,"Accelerometer Sensor","X Axis: "+x+"\nY Axis: "+y+"\nZ Axis: "+z);
                }
            }
            else if (currentSensor == Sensor.TYPE_GYROSCOPE) {

                if (event.values[2] > 0.5f) {
                    textView.setText("Rotated Anti Clock Wise");
                    textView2.setText("Gyroscope Sensor");
//                    customDialog(this,"Gyroscope Sensor","You Rotated your device Anti Clockwise");
                }
                else if (event.values[2] < -0.5f) {
                    textView.setText("Rotated Clockwise");
                    textView2.setText("Gyroscope Sensor");
//                    customDialog(this,"Gyroscope Sensor","You Rotated your device Clockwise");
                } else {
                    textView2.setText("Gyroscope Sensor");
                    textView.setText("Rotate your Device");
//                    customDialog(this,"Gyroscope Sensor","Rotate your device");
                }
            }
            else if (currentSensor == Sensor.TYPE_AMBIENT_TEMPERATURE) {
                textView.setText("Ambient Temperature in Celsius: " + event.values[0]);
                textView2.setText("Ambient Temperature Sensor");
//                customDialog(this,"Ambient Temperature Sensor","Ambient Temperature in Celsius: "+ event.values[0]);
            }
            else if (currentSensor == Sensor.TYPE_ROTATION_VECTOR){
                textView.setText("Rotated " +event.values[0]+" Degree");
                textView2.setText("Rotation Sensor");
//                customDialog(this,"Rotation Sensor","Rotated "+ event.values[0]+" Degree");
            }
            else if (currentSensor == Sensor.TYPE_LINEAR_ACCELERATION){
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];

                textView2.setText("Linear Acceleration Sensor");
                textView.setText("X Axis: "+x+"\nY Axis: "+y+"\nZ Axis "+z);
//                customDialog(this,"Linear Acceleration Sensor","X Axis: "+x+"\nY Axis: "+y+"\nZ Axis: "+z);

                long curTime = System.currentTimeMillis();

                if ((curTime - lastUpdate) > 100) {
                    long diffTime = (curTime - lastUpdate);
                    lastUpdate = curTime;

                    float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;

                    if (speed > SHAKE_THRESHOLD) {
                        Toast.makeText(getApplicationContext(), "Your phone just shook", Toast.LENGTH_LONG).show();
                    }

                    last_x = x;
                    last_y = y;
                    last_z = z;
                }
            }
            else if (currentSensor == Sensor.TYPE_GRAVITY){
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];

                textView2.setText("Gravity Sensor");
                textView.setText("X Axis: "+x+"\nY Axis: "+y+"\nZ Axis "+z);
            }

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @SuppressLint("SetTextI18n")
    public void accelerometerSensorOnClick(View view) {
        if (checkSensorAvailability(Sensor.TYPE_ACCELEROMETER)) {
            currentSensor = Sensor.TYPE_ACCELEROMETER;
        } else {
            Popup.Companion.error(this,"Not Available","Accelerometer Sensor is not Available on your Phone",R.font.comfortaa);
        }
    }

    @SuppressLint("SetTextI18n")
    public void proximitySensorOnClick(View view) {
        if (checkSensorAvailability(Sensor.TYPE_PROXIMITY)) {
            currentSensor = Sensor.TYPE_PROXIMITY;
        } else {
            Popup.Companion.error(this,"Not Available","Proximity Sensor is not Available on your Phone",R.font.comfortaa);
        }

    }

    @SuppressLint("SetTextI18n")
    public void gyroscopeSensorOnClick(View view) {
        if (checkSensorAvailability(Sensor.TYPE_GYROSCOPE)) {
            currentSensor = Sensor.TYPE_GYROSCOPE;
        } else {
            Popup.Companion.error(this,"Not Available","Gyroscope Sensor is not Available on your Phone",R.font.comfortaa);
        }
    }

    @SuppressLint("SetTextI18n")
    public void lightSensorOnClick(View view) {
        if (checkSensorAvailability(Sensor.TYPE_LIGHT)) {
            currentSensor = Sensor.TYPE_LIGHT;
        } else {
            Popup.Companion.error(this,"Not Available","Light Sensor is not Available on your Phone",R.font.comfortaa);
        }
    }

    @SuppressLint("SetTextI18n")
    public void stepCounterOnClick(View view) {
        if (checkSensorAvailability(Sensor.TYPE_STEP_COUNTER)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                currentSensor = Sensor.TYPE_STEP_COUNTER;
            } else{
                Toast.makeText(this,"Not Available in this SDK",Toast.LENGTH_LONG).show();
            }
        } else {
            Popup.Companion.error(this,"Not Available","Step Counter Sensor is not Available on your Phone",R.font.comfortaa);
        }
    }

    @SuppressLint("SetTextI18n")
    public void ambientTempSensorOnClick(View view) {
        if (checkSensorAvailability(Sensor.TYPE_AMBIENT_TEMPERATURE)) {
            currentSensor = Sensor.TYPE_AMBIENT_TEMPERATURE;
        } else {
            Popup.Companion.error(this,"Not Available","Ambient Temperature Sensor is not Available on your Phone",R.font.comfortaa);
        }
    }

    @SuppressLint("SetTextI18n")
    public void rotationSensorOnClick(View view) {
        if (checkSensorAvailability(Sensor.TYPE_ROTATION_VECTOR)){
            currentSensor = Sensor.TYPE_ROTATION_VECTOR;
        } else {
            Popup.Companion.error(this,"Not Available","Rotate Sensor is not Available on your Phone",R.font.comfortaa);
        }
    }

    public void ambbientLightSensorOnClick(View view) {

        if (checkSensorAvailability((Sensor.TYPE_LINEAR_ACCELERATION))){
            currentSensor = Sensor.TYPE_LINEAR_ACCELERATION;
        } else {
            Popup.Companion.error(this,"Not Available","Linear Acceleration Sensor is not Available on your Phone",R.font.comfortaa);
        }
    }

    public void gravitySensorOnClick(View view) {
        if (checkSensorAvailability(Sensor.TYPE_GRAVITY)){
            currentSensor = Sensor.TYPE_GRAVITY;
        }else {
            Popup.Companion.error(this,"Not Available","Gravity Sensor is not available in your device",R.font.comfortaa);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        run = true;

        sensorManager.registerListener(this, accelerometerSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, lightSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, proximitySensor,
                SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, stepCounterSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, tempSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, gyroscopeSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this,rotationSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this,linearAccelerationSensor,SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this,gravitySensor,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }


    @Override
    public void onBackPressed() {

        Popup.Companion.exit(this,"Close Sensor App?","Do you really want to exit?");
    }

    private void customDialog(final Activity activity, String heading, String title){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_sensor);

        TextView headingTV = dialog.findViewById(R.id.tv_heading);
        headingTV.setText(heading);

        TextView titleTV = dialog.findViewById(R.id.tv_title);
        titleTV.setText(title);

        Button okButton = dialog.findViewById(R.id.btn_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
